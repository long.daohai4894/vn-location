from sqlalchemy import Column, String, Text

from app.models.model_base import BareBaseModel


class Province(BareBaseModel):
    province_code = Column(String, index=True, unique=True, comment='Mã')
    province_name = Column(String, index=True, comment='Tên')
    province_name_eng = Column(String, index=True, comment='Tên Tiếng Anh')
    level = Column(Text, nullable=True, comment='Cấp')

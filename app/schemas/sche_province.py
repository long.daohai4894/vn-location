from typing import Optional, List

from pydantic import BaseModel, Field

from app.schemas.sche_base import ResponseWithTotal


class ProvinceRequest(BaseModel):
    province_name: Optional[str] = Field(None, alias='provinceName')


class ProvinceResponse(ResponseWithTotal):
    class ProvinceItem(BaseModel):
        class Config:
            arbitrary_types_allowed = True

        id: int
        province_code: str
        province_name: str
        province_name_eng: Optional[str]
        level: str

    provinces: List[ProvinceItem]

from sqlalchemy import Column, String, Text

from app.models.model_base import BareBaseModel


class District(BareBaseModel):
    district_code = Column(String, index=True, unique=True, comment='Mã')
    district_name = Column(String, index=True, comment='Tên')
    district_name_eng = Column(String, index=True, comment='Tên Tiếng Anh')
    level = Column(Text, nullable=True, comment='Cấp')
    province_code = Column(String, index=True, comment='Mã TP')
    province_name = Column(String, index=True, comment='Tỉnh Thành Phố')

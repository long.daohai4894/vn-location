from typing import Optional, List

from pydantic import BaseModel, Field

from app.schemas.sche_base import ResponseWithTotal


class WardRequest(BaseModel):
    ward_name: Optional[str] = Field(None, alias='wardName')


class WardResponse(ResponseWithTotal):
    class WardItem(BaseModel):
        id: int
        ward_code: str
        ward_name: str
        ward_name_eng: Optional[str]
        level: str

    wards: List[WardItem]

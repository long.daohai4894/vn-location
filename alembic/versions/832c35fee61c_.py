"""empty message

Revision ID: 832c35fee61c
Revises: 
Create Date: 2021-07-25 23:03:37.777146

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '832c35fee61c'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('district',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('district_code', sa.String(), nullable=True, comment='Mã'),
    sa.Column('district_name', sa.String(), nullable=True, comment='Tên'),
    sa.Column('district_name_eng', sa.String(), nullable=True, comment='Tên Tiếng Anh'),
    sa.Column('level', sa.Text(), nullable=True, comment='Cấp'),
    sa.Column('province_code', sa.String(), nullable=True, comment='Mã TP'),
    sa.Column('province_name', sa.String(), nullable=True, comment='Tỉnh Thành Phố'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_district_district_code'), 'district', ['district_code'], unique=True)
    op.create_index(op.f('ix_district_district_name'), 'district', ['district_name'], unique=False)
    op.create_index(op.f('ix_district_district_name_eng'), 'district', ['district_name_eng'], unique=False)
    op.create_index(op.f('ix_district_province_code'), 'district', ['province_code'], unique=False)
    op.create_index(op.f('ix_district_province_name'), 'district', ['province_name'], unique=False)
    op.create_table('province',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('province_code', sa.String(), nullable=True, comment='Mã'),
    sa.Column('province_name', sa.String(), nullable=True, comment='Tên'),
    sa.Column('province_name_eng', sa.String(), nullable=True, comment='Tên Tiếng Anh'),
    sa.Column('level', sa.Text(), nullable=True, comment='Cấp'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_province_province_code'), 'province', ['province_code'], unique=True)
    op.create_index(op.f('ix_province_province_name'), 'province', ['province_name'], unique=False)
    op.create_index(op.f('ix_province_province_name_eng'), 'province', ['province_name_eng'], unique=False)
    op.create_table('ward',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('ward_code', sa.String(), nullable=True, comment='Mã'),
    sa.Column('ward_name', sa.String(), nullable=True, comment='Tên'),
    sa.Column('ward_name_eng', sa.String(), nullable=True, comment='Tên Tiếng Anh'),
    sa.Column('level', sa.Text(), nullable=True, comment='Cấp'),
    sa.Column('district_code', sa.String(), nullable=True, comment='Mã QH'),
    sa.Column('district_name', sa.String(), nullable=True, comment='Quận Huyện'),
    sa.Column('province_code', sa.String(), nullable=True, comment='Mã TP'),
    sa.Column('province_name', sa.String(), nullable=True, comment='Tỉnh Thành Phố'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_ward_district_code'), 'ward', ['district_code'], unique=False)
    op.create_index(op.f('ix_ward_district_name'), 'ward', ['district_name'], unique=False)
    op.create_index(op.f('ix_ward_province_code'), 'ward', ['province_code'], unique=False)
    op.create_index(op.f('ix_ward_province_name'), 'ward', ['province_name'], unique=False)
    op.create_index(op.f('ix_ward_ward_code'), 'ward', ['ward_code'], unique=True)
    op.create_index(op.f('ix_ward_ward_name'), 'ward', ['ward_name'], unique=False)
    op.create_index(op.f('ix_ward_ward_name_eng'), 'ward', ['ward_name_eng'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_ward_ward_name_eng'), table_name='ward')
    op.drop_index(op.f('ix_ward_ward_name'), table_name='ward')
    op.drop_index(op.f('ix_ward_ward_code'), table_name='ward')
    op.drop_index(op.f('ix_ward_province_name'), table_name='ward')
    op.drop_index(op.f('ix_ward_province_code'), table_name='ward')
    op.drop_index(op.f('ix_ward_district_name'), table_name='ward')
    op.drop_index(op.f('ix_ward_district_code'), table_name='ward')
    op.drop_table('ward')
    op.drop_index(op.f('ix_province_province_name_eng'), table_name='province')
    op.drop_index(op.f('ix_province_province_name'), table_name='province')
    op.drop_index(op.f('ix_province_province_code'), table_name='province')
    op.drop_table('province')
    op.drop_index(op.f('ix_district_province_name'), table_name='district')
    op.drop_index(op.f('ix_district_province_code'), table_name='district')
    op.drop_index(op.f('ix_district_district_name_eng'), table_name='district')
    op.drop_index(op.f('ix_district_district_name'), table_name='district')
    op.drop_index(op.f('ix_district_district_code'), table_name='district')
    op.drop_table('district')
    # ### end Alembic commands ###

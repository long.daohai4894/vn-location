from typing import Type, TypeVar
from app.models import Base

ModelType = TypeVar("ModelType", bound=Base)


class BaseService:
    def __init__(self, model: Type[ModelType]):
        self.model = model

from fastapi_sqlalchemy import db

from app.models import District
from app.services.srv_base import BaseService


class DistrictService(BaseService):
    def __init__(self):
        super().__init__(District)

    def get_lists(self, provinceCode: str, district_name: str) -> dict:
        _query = db.session.query(self.model).filter(District.province_code == provinceCode)
        if district_name:
            _query = _query.filter(self.model.district_name.ilike(f'%{district_name}%'))
        data = []
        for item in _query.all():
            data.append(item.__dict__)
        return {
            'data': data,
            'total': _query.count()
        }

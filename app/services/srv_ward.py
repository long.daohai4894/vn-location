from fastapi_sqlalchemy import db

from app.models import Ward
from app.services.srv_base import BaseService


class WardService(BaseService):
    def __init__(self):
        super().__init__(Ward)

    def get_lists(self, districtCode: str, ward_name: str):
        _query = db.session.query(self.model).filter(Ward.district_code == districtCode)
        if ward_name:
            _query = _query.filter(self.model.ward_name.ilike(f'%{ward_name}%'))
        data = []
        for item in _query.all():
            data.append(item.__dict__)
        return {
            'data': data,
            'total': _query.count()
        }

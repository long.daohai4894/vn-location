from sqlalchemy import Column, String, Text

from app.models.model_base import BareBaseModel


class Ward(BareBaseModel):
    ward_code = Column(String, index=True, unique=True, comment='Mã')
    ward_name = Column(String, index=True, comment='Tên')
    ward_name_eng = Column(String, index=True, comment='Tên Tiếng Anh')
    level = Column(Text, nullable=True, comment='Cấp')
    district_code = Column(String, index=True, comment='Mã QH')
    district_name = Column(String, index=True, comment='Quận Huyện')
    province_code = Column(String, index=True, comment='Mã TP')
    province_name = Column(String, index=True, comment='Tỉnh Thành Phố')

from typing import Optional, List

from pydantic import BaseModel, Field

from app.schemas.sche_base import ResponseWithTotal


class DistrictRequest(BaseModel):
    district_name: Optional[str] = Field(None, alias='districtName')


class DistrictResponse(ResponseWithTotal):
    class DistrictItem(BaseModel):
        id: int
        district_code: str
        district_name: str
        district_name_eng: Optional[str]
        level: str

    districts: List[DistrictItem]

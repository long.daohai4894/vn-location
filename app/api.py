import logging

from fastapi import APIRouter, Depends

from app.schemas.sche_base import DataResponse
from app.schemas.sche_district import DistrictResponse, DistrictRequest
from app.schemas.sche_province import ProvinceResponse, ProvinceRequest
from app.schemas.sche_ward import WardRequest, WardResponse
from app.services.srv_district import DistrictService
from app.services.srv_province import ProvinceService
from app.services.srv_ward import WardService

logger = logging.getLogger()
router = APIRouter(tags=['Location'])


@router.get("/provinces", response_model=DataResponse[ProvinceResponse])
def get_provinces(province_list_req: ProvinceRequest = Depends()):
    """
    API Get list Province
    """
    data = ProvinceService().get_lists(req=province_list_req)
    return DataResponse().success_response(data={'provinces': data['data'], 'total': data['total']})


@router.get("/districts/{provinceCode}", response_model=DataResponse[DistrictResponse])
def get_districts(provinceCode: str, req_data: DistrictRequest = Depends()):
    """
    API Get list District
    """
    data = DistrictService().get_lists(provinceCode=provinceCode, district_name=req_data.district_name)
    return DataResponse().success_response(data={'districts': data['data'], 'total': data['total']})


@router.get("/wards/{districtCode}", response_model=DataResponse[WardResponse])
def get_wards(districtCode: str, req_data: WardRequest = Depends()):
    """
    API Get list Ward
    """
    data = WardService().get_lists(districtCode=districtCode, ward_name=req_data.ward_name)
    return DataResponse().success_response(data={'wards': data['data'], 'total': data['total']})

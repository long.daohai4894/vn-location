# Import all the models, so that Base has them before being
# imported by Alembic
from app.models.model_base import Base  # noqa
from app.models.model_province import Province
from app.models.model_district import District
from app.models.model_ward import Ward

from fastapi_sqlalchemy import db

from app.models import Province
from app.schemas.sche_province import ProvinceRequest
from app.services.srv_base import BaseService


class ProvinceService(BaseService):
    def __init__(self):
        super().__init__(Province)

    def get_lists(self, req: ProvinceRequest):
        _query = db.session.query(self.model)
        if req.province_name:
            _query = _query.filter(self.model.province_name.ilike(f'%{req.province_name}%'))
        data = []
        for item in _query.all():
            data.append(item.__dict__)
        return {
            'data': data,
            'total': _query.count()
        }
